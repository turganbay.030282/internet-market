<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        return response()->json('', Response::HTTP_OK);
    }

    public function create()
    {
        return CategoryResource::make(new Category());
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['parent_id'] = filter_var($request->get('parent_id'), FILTER_VALIDATE_INT);
        $category = Category::query()->create($data);
        if ($data['parent_id']) {
            $categoryParent = Category::query()->find($data['parent_id']);
            $category->prependToNode($categoryParent)->save();
            $category->save();
        }
        return response()->json($category, Response::HTTP_CREATED);
    }

    public function edit($id)
    {
        return response()->json(['data' => Category::query()->findOrFail($id)], Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $category = Category::query()->find($id);
        $category->name = $request->get('name');
        $category->parent_id = filter_var($request->get('parent_id'), FILTER_VALIDATE_INT);
        $category->save();
        return response()->json($category, Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $category = Category::query()->find($id);
        $category->delete();
        return response()->json([], Response::HTTP_OK);
    }
}
